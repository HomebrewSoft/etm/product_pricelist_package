# -*- coding: utf-8 -*-
{
    "name": "Product Pricelist Package",
    "version": "13.0.0.0.2.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev/",
    'license': 'LGPL-3',
    "depends": [
        "plan_production_order",  # https://gitlab.com/HomebrewSoft/etm/plan_production_order
        "product",
        "sale_stock",
        "sale",
        "stock",
    ],
    "data": [
        # security
        # data
        # reports
        "reports/stock_report_stockpicking_operations.xml",
        "reports/stock_report_delivery_document.xml",
        # views
        "views/mrp_production.xml",
        "views/product_pricelist.xml",
        "views/stock_picking.xml",
        "views/product_pricelist_item.xml",
        "views/stock_quant.xml",
    ],
}
