from odoo import _, api, fields, models, fields


class Production(models.Model):
    _inherit = "mrp.production"

    sale_line_id = fields.Many2one(
        comodel_name="sale.order.line",
        compute="_compute_sale_line_id",
        store=True,
    )
    sale_uom_id = fields.Many2one(
        string="Sale UoM",
        related="sale_line_id.product_uom",
    )
    sale_qty = fields.Float(
        string="Sale Qty",
        related="sale_line_id.product_uom_qty",
    )

    @api.depends("sale_id", "product_id")
    def _compute_sale_line_id(self):
        for production in self:
            production.sale_line_id = []
            if not production.sale_id:
                continue
            lines = production.sale_id.order_line.filtered(
                lambda l: production.product_id == l.product_id
            )
            production.sale_line_id = lines[0:1]
