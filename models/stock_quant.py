# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class StockQuant(models.Model):
    _inherit = "stock.quant"

    sale_uom_id = fields.Many2one(
        string="Sale UoM",
        related="lot_id.sale_uom_id",
    )
    sale_qty = fields.Float(
        string="Sale Qty",
        related="lot_id.sale_qty",
    )
