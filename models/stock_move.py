# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class StockMove(models.Model):
    _inherit = "stock.move"

    sale_uom_id = fields.Many2one(
        string="Sale UoM",
        related="sale_line_id.product_uom",
    )

    sale_qty = fields.Float(
        string="Sale Qty",
        related="sale_line_id.product_uom_qty",
    )
