from odoo import _, api, fields, models


class ProductProduce(models.TransientModel):
    _inherit = "mrp.product.produce"

    def action_generate_serial(self):
        res = super(ProductProduce, self).action_generate_serial(self)
        self.finished_lot_id.production_id = self.production_id
        return res
