# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProductPricelistItem(models.Model):
    _inherit = "product.pricelist.item"

    uom_id = fields.Many2one(
        comodel_name="uom.uom",
    )
    product_uom_category_id = fields.Many2one(
        related="product_id.uom_id.category_id",
    )
    template_uom_category_id = fields.Many2one(
        related="product_tmpl_id.uom_id.category_id",
    )
    package_price = fields.Monetary(
        required=True,
    )
    package_to_base_units = fields.Float(
        compute="_compute_package_to_base_units",
    )
    fixed_price = fields.Float(
        compute="_compute_fixed_price",
        store=True,
    )

    @api.depends("product_id.uom_id", "product_tmpl_id.uom_id", "uom_id")
    def _compute_package_to_base_units(self):
        for item in self:
            item.package_to_base_units = 1
            product = item.product_id or item.product_tmpl_id
            if not product:
                continue
            if not self.uom_id:
                continue
            item.package_to_base_units = product.uom_id._compute_quantity(
                qty=1, to_unit=item.uom_id
            )

    @api.depends("package_price", "package_to_base_units")
    def _compute_fixed_price(self):
        for item in self:
            item.fixed_price = item.package_price * item.package_to_base_units
