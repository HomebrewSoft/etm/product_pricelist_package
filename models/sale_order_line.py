# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    def _get_rule(self):
        if not (self.product_id and self.order_id.pricelist_id):
            return None
        _price, rule_id = self.order_id.pricelist_id.get_product_price_rule(
            self.product_id, self.product_uom_qty or 1.0, self.order_id.partner_id
        )
        rule = rule_id and self.env["product.pricelist.item"].browse(rule_id) or None
        return rule

    def _set_uom(self):
        rule = self._get_rule()
        if rule:
            self.product_uom = rule.uom_id

    @api.onchange("product_id")
    def product_id_change(self):
        res = super(SaleOrderLine, self).product_id_change()
        self._set_uom()
        return res
