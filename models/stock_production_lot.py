from odoo import _, api, fields, models


class StockProductionLot(models.Model):
    _inherit = "stock.production.lot"

    production_id = fields.Many2one(
        comodel_name="mrp.production",
    )
    sale_uom_id = fields.Many2one(
        string="Sale UoM",
        related="production_id.sale_uom_id",
    )
    sale_qty = fields.Float(
        string="Sale Qty",
        related="production_id.sale_qty",
    )
